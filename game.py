
#declaring variables
board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board
def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

#winning_board function prints winning board and player information
#called when game loop finds winning board
def winning_board():
    print_board(current_board)
    print("Congratulations Player", current_player, "You won!")
    exit()

#printing the initial board layout
current_board = board

#this loop will show the current board, prompt the user for next move response,
#update the board, check the board for winning conditions (calling winning_board function)
#and alternate the current player - repeating for 10 turns
for turns in range(0, 9):
    print_board(current_board)
    print("Player", current_player, "where would you like to place your marker?")
    response = input("Type an available number 1 through 9\n")
    response = int(response)
    current_board[response - 1] = current_player

    if (current_board[0] == current_board[1] and current_board[1] == current_board[2]):
        winning_board()
    elif (current_board[3] == current_board[4] and current_board[4] == current_board[5]):
        winning_board()
    elif (current_board[6] == current_board[7] and current_board[7] == current_board[8]):
        winning_board()
    elif (current_board[0] == current_board[4] and current_board[4] == current_board[8]):
        winning_board()
    elif (current_board[6] == current_board[4] and current_board[4] == current_board[2]):
        winning_board()
    elif (current_board[0] == current_board[3] and current_board[3] == current_board[6]):
        winning_board()
    elif (current_board[1] == current_board[4] and current_board[4] == current_board[7]):
        winning_board()
    elif (current_board[2] == current_board[5] and current_board[5] == current_board[8]):
        winning_board()    

    if (current_player == "X"):
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie! No winners here. Play again soon!")


    #stretch goals
    #have computer play 'O' against you by finding an empty space
    #on the board - this would be another for loop nested inside 
    #for loop that makes the game work